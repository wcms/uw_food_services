<?php

/**
 * @file
 */

$plugin = [
  'label' => t('Food Services - Franchises'),
  'resource' => 'franchises',
  'name' => 'franchises__1_0',
  'entity_type' => 'node',
  'bundle' => 'uw_ct_franchise',
  'description' => t('Export the franchises with all authentication providers.'),
  'class' => 'RestfulEntityNodeFranchises',
  'authentication_types' => TRUE,
  'authentication_optional' => TRUE,
  'major_version' => 1,
  'minor_version' => 0,
];
