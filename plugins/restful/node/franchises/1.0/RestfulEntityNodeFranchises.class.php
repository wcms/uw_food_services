<?php

/**
 * @file
 * Contains RestfulEntityNodeFood.
 */

/**
 *
 */
class RestfulEntityNodeFranchises extends UWRestfulEntityNode {

  /**
   * Overrides Food Franchises content type public fields.
   *
   * @return array
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    // Adding mapping array, where key is api response label, and value is
    // field name if provided as string, or it could be an array with
    // declaration for public field.
    $mapping = [
      'name' => 'title_field',
      'logo' => [
        'property' => 'field_franchise_logo',
        'process_callbacks' => [
          [$this, 'imageProcess'],
        ],
      ],
      'combos' => [
        'property' => 'field_combos',
        'process_callbacks' => [
          [$this, 'processCombos'],
        ],
      ],
      'individual items' => [
        'property' => 'field_individual_items',
        'process_callbacks' => [
          [$this, 'processIndividualItems'],
        ],
        // 'resource' => [
        //          'franchise_individual' => 'franchise_individual',
        //        ],
      ],
      'addons' => [
        'property' => 'field_addons',
        'process_callbacks' => [
          [$this, 'processAddons'],
        ],
        // 'resource' => [
        //          'franchise_addons' => 'franchise_addons',
        //        ],
      ],
    ];

    foreach ($mapping as $label => $value) {
      if (is_array($value)) {
        $public_fields[$label] = $value;
      }
      else {
        $public_fields[$label] = [
          'property' => $value,
        ];
      }
    }

    return $public_fields;
  }

  /**
   * Processes paragraph.
   *
   * @param $values
   *   Entity data wrapper for node.
   *
   * @return array
   *   Output for json.
   */
  protected function processCombos($values) {
    $output = [];

    foreach ($values as $item) {
      $image = $item->field_combo_image;
      $output[] = [
        'name' => $item->field_combo_name[LANGUAGE_NONE][0]['value'],
        'image' => $image ? static::imageProcess($image[LANGUAGE_NONE][0]) : '',
        'options' => $item->field_combo_options[LANGUAGE_NONE][0]['value'],
        'item' => $item->field_combo_other[LANGUAGE_NONE][0]['value'],
        'price' => $item->field_combo_price[LANGUAGE_NONE][0]['value'],
        'calories' => $item->field_combo_calories[LANGUAGE_NONE][0]['value'],
      ];
    }

    return $output;
  }

  /**
   * Process Individual items for franchise.
   *
   * @param $values
   *   Array of items to process.
   *
   * @return array
   *   List ready to display.
   */
  protected function processIndividualItems($values) {
    $output = [];

    foreach ($values as $item) {
      $image = $item->field_individual_photo;

      $output[] = [
        'name' => $item->field_individual_name[LANGUAGE_NONE][0]['value'],
        'photo' => $image ? static::imageProcess($image[LANGUAGE_NONE][0]) : '',
        'price' => $item->field_individual_price[LANGUAGE_NONE][0]['value'],
        'calories' => $item->field_individual_calories[LANGUAGE_NONE][0]['value'],
      ];
    }

    return $output;
  }

  /**
   * Process Addons for franchise.
   *
   * @param $values
   *   Array of items.
   *
   * @return array
   *   Ready to display.
   */
  protected function processAddons($values) {
    $output = [];

    foreach ($values as $item) {
      $image = $item->field_addon_photo;

      $output[] = [
        'name' => $item->field_addon_name[LANGUAGE_NONE][0]['value'],
        'photo' => $image ? static::imageProcess($image[LANGUAGE_NONE][0]) : '',
        'price' => $item->field_addon_price[LANGUAGE_NONE][0]['value'],
        'calories' => $item->field_addon_calories[LANGUAGE_NONE][0]['value'],
      ];
    }

    return $output;
  }

}
