<?php

/**
 * @file
 */

$plugin = [
  'label' => t('Food Services - Food'),
  'resource' => 'food',
  'name' => 'food__1_0',
  'entity_type' => 'node',
  'bundle' => 'uw_ct_food',
  'description' => t('Export the food content with all authentication providers.'),
  'class' => 'RestfulEntityNodeFood',
  'authentication_types' => TRUE,
  'authentication_optional' => TRUE,
  'major_version' => 1,
  'minor_version' => 0,
];
