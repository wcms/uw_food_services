<?php

/**
 * @file
 * Contains RestfulEntityNodeFood.
 */

/**
 *
 */
class RestfulEntityNodeFood extends UWRestfulEntityNode {

  /**
   * Overrides Food content type public fields.
   *
   * @return array
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    // Adding mapping array, where key is api response label, and value is
    // field name if provided as string, or it could be an array with
    // declaration for public field.
    $mapping = [
      'diettype1' => [
        'property' => 'field_diet_type',
        'resource' => [
          'diet_type' => 'diet_type',
        ],
      ],
      'diettype2' => [
        'property' => 'field_uw_ct_food_diet_type',
        'resource' => [
          'dietary_type' => 'dietary_type',
        ],
      ],
      'contains' => [
        'property' => 'field_allergens',
        'resource' => [
          'allergens' => 'allergens',
        ],
      ],
      'serving size (g)' => 'field_serving_size',
      'ingredients' => 'field_ingredients',
      'calories' => 'field_calories',
      'fat (g)' => 'field_amount_fat',
      'fat daily %' => 'field_daily_value_fat',
      'fat saturated (g)' => 'field_amount_saturated',
      'fat saturated daily %' => 'field_daily_value_saturated',
      'fat trans (g)' => 'field_amount_trans',
      'cholesterol (g)' => 'field_cholesterol',
      'cholesterol daily %' => 'field_daily_value_carbohydrate',
      'fibre (g)' => 'field_amount_fibre',
      'fibre daily %' => 'field_daily_value_fibre',
      'sugars (g)' => 'field_sugars',
      'protein (g)' => 'field_protein',
      'vitamin a daily %' => 'field_vitamin_a',
      'vitamin c daily %' => 'field_vitamin_c',
      'calcium daily %' => 'field_calcium',
      'iron daily %' => 'field_iron',
    ];

    foreach ($mapping as $label => $value) {
      if (is_array($value)) {
        $public_fields[$label] = $value;
      }
      else {
        $public_fields[$label] = [
          'property' => $value,
        ];
      }
    }

    return $public_fields;
  }

}
