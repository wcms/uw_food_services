<?php

/**
 * @file
 * Contains RestfulEntityNodeFood.
 */

/**
 *
 */
class RestfulEntityNodeOutlets extends UWRestfulEntityNode {

  /**
   * Overrides Food Franchises content type public fields.
   *
   * @return array
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    // Adding mapping array, where key is api response label, and value is
    // field name if provided as string, or it could be an array with
    // declaration for public field.
    $mapping = [
      'image' => [
        'property' => 'field_image',
        'process_callbacks' => [
          [$this, 'imageProcess'],
        ],
      ],
      'photo' => [
        'property' => 'field_outlet_photo',
        'process_callbacks' => [
          [$this, 'imageProcess'],
        ],
      ],
      'features' => [
        'property' => 'field_features',
        'sub_property' => 'value',
        'process_callbacks' => [
          'strip_tags', 'trim',
        ],
      ],
      'outlet type' => [
        'property' => 'field_outlet_type',
      // 'resource' => [
      //          'outlettype' => 'outlettype',
      //        ],
        'process_callbacks' => [
          [$this, 'processOutletType'],
        ],
      ],
      'outlet location' => 'field_outlet_location',
      'location link' => 'field_uw_fs_location_link',
      'description' => [
        'property' => 'field_description',
        'sub_property' => 'value',
        'process_callbacks' => [
          'strip_tags', 'trim',
        ],
      ],
      'location' => [
        'property' => 'field_location',
        'callback' => [
          $this, 'processLocation',
        ],
      ],
      'summary' => 'field_outlet_summary',
      'cuisine' => [
        'property' => 'field_serves_cuisine',
        'resource' => [
          'cuisines' => 'cuisines',
        ],
      ],
      'opening hours' => 'field_opening_hours',
      'payment accepted' => [
        'property' => 'field_payment_accepted',
        'process_callbacks' => [
          [$this, 'processPayments'],
        ],
        // 'resource' => [
        //          'payments' => 'payments',
        //        ],
      ],
      'closed' => 'field_date_closed',
      'hours change' => 'field_hours_change',
      'notice' => 'field_hours_notice',
      'franchise menu' => 'field_uw_fo_franchise_menus',
      'sticky' => 'sticky',
      'created' => 'created',
    ];

    foreach ($mapping as $label => $value) {
      if (is_array($value)) {
        $public_fields[$label] = $value;
      }
      else {
        $public_fields[$label] = [
          'property' => $value,
        ];
      }
    }

    return $public_fields;
  }

  /**
   * Filter Outlet type values.
   *
   * @param array|mixed $values
   *   Array of values containing outlet types.
   *
   * @return array
   *   Returning tid, name and description.
   */
  protected function processOutletType($values) {
    $output = [];

    foreach ($values as $item) {
      $output[] = [
        'tid' => $item->tid,
        'name' => $item->name,
        'description' => trim(strip_tags($item->description)),
      ];
    }

    return $output;
  }

  /**
   * Cleans up payment values.
   *
   * @param $values
   *   Input values.
   *
   * @return array
   *   Array payments output.
   */
  protected function processPayments($values) {
    $output = [];

    foreach ($values as $item) {
      $output[] = [
        'tid' => $item->tid,
        'name' => $item->name,
      ];
    }

    return $output;
  }

  /**
   * Returns location data.
   *
   * @param $wrapper
   *   Entity metadata wrapper of node.
   *
   * @return array
   *   Array of location data.
   */
  protected function processLocation($wrapper) {
    // Getting node from wrapper, since getting location value from wrapper
    // doesn't work ($wrapper->field_location->value()) is throwing error.
    $node = $wrapper->raw();

    // If location is not empty, return array, this field may hold multiple
    // values.
    if (isset($node->field_location[LANGUAGE_NONE])) {
      return $node->field_location[LANGUAGE_NONE];
    }

    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function defaultSortInfo() {
    return [
      'sticky' => 'DESC',
      'name' => 'ASC',
      'created' => 'DESC',
    ];
  }

}
