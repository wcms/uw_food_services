<?php

/**
 * @file
 */

$plugin = [
  'label' => t('Food Services - Outlets'),
  'resource' => 'outlets',
  'name' => 'outlets__1_0',
  'entity_type' => 'node',
  'bundle' => 'uw_food_outlet',
  'description' => t('Export the outlets with all authentication providers.'),
  'class' => 'RestfulEntityNodeOutlets',
  'authentication_types' => TRUE,
  'authentication_optional' => TRUE,
  'major_version' => 1,
  'minor_version' => 0,
];
