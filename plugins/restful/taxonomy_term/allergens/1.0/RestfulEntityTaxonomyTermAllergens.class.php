<?php

/**
 * @file
 * Contains \RestfulEntityTaxonomyTermAllergens.
 */

/**
 *
 */
class RestfulEntityTaxonomyTermAllergens extends UWRestfulEntityTaxonomyTerm {

  /**
   * {@inheritDoc}
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    $public_fields['description'] = $this->getTaxonomyDescription();

    return $public_fields;
  }

}
