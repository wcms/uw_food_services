<?php

/**
 * @file
 */

$plugin = [
  'label' => t('Food Services - Allergens'),
  'resource' => 'allergens',
  'name' => 'allergens__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'allergens',
  'description' => t('Export the allergen types with all authentication providers.'),
  'class' => 'RestfulEntityTaxonomyTermAllergens',
  'authentication_types' => TRUE,
  'authentication_optional' => TRUE,
  'major_version' => 1,
  'minor_version' => 0,
];
