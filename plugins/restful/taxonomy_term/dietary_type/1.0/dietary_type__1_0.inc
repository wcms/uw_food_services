<?php

/**
 * @file
 */

$plugin = [
  'label' => t('Food Services - Dietary Types'),
  'resource' => 'dietary_type',
  'name' => 'dietary_type__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uw_fs_dietary_types',
  'description' => t('Export the dietary types with all authentication providers.'),
  'class' => 'RestfulEntityTaxonomyTermDietaryTypes',
  'authentication_types' => TRUE,
  'authentication_optional' => TRUE,
  'major_version' => 1,
  'minor_version' => 0,
];
