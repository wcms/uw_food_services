<?php

/**
 * @file
 * Contains \RestfulEntityTaxonomyTermDietaryTypes.
 */

/**
 *
 */
class RestfulEntityTaxonomyTermDietaryTypes extends UWRestfulEntityTaxonomyTerm {

  /**
   * {@inheritDoc}
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    $public_fields['description'] = $this->getTaxonomyDescription();

    $public_fields['icon'] = [
      'sub_property' => 'field_uw_fs_dietary_type_icon',
      'wrapper_method' => 'value',
      'wrapper_method_on_entity' => TRUE,
      'process_callbacks' => [
        [$this, 'imageProcess'],
      ],
    ];

    return $public_fields;
  }

}
