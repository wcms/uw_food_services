<?php

/**
 * @file
 */

$plugin = [
  'label' => t('Food Services - Payments Types'),
  'resource' => 'payments',
  'name' => 'payments__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uw_payment_type',
  'description' => t('Export the payment types types with all authentication providers.'),
  'class' => 'RestfulEntityTaxonomyTermPaymentTypes',
  'authentication_types' => TRUE,
  'authentication_optional' => TRUE,
  'major_version' => 1,
  'minor_version' => 0,
];
