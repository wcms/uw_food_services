<?php

/**
 * @file
 * Contains \RestfulEntityTaxonomyTermPaymentTypes.
 */

/**
 *
 */
class RestfulEntityTaxonomyTermPaymentTypes extends UWRestfulEntityTaxonomyTerm {

  /**
   * {@inheritDoc}
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    $public_fields['description'] = $this->getTaxonomyDescription();

    return $public_fields;
  }

}
