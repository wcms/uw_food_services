<?php

/**
 * @file
 */

$plugin = [
  'label' => t('Food Services - Diet Types'),
  'resource' => 'diet_type',
  'name' => 'diet_type__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uw_diet_type',
  'description' => t('Export the diet types with all authentication providers.'),
  'class' => 'RestfulEntityTaxonomyTermDietType',
  'authentication_types' => TRUE,
  'authentication_optional' => TRUE,
  'major_version' => 1,
  'minor_version' => 0,
];
