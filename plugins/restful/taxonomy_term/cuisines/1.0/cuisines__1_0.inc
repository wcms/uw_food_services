<?php

/**
 * @file
 */

$plugin = [
  'label' => t('Food Services - Cuisines'),
  'resource' => 'cuisines',
  'name' => 'cuisines__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'cuisines',
  'description' => t('Export the cuisines types with all authentication providers.'),
  'class' => 'RestfulEntityTaxonomyTermCuisines',
  'authentication_types' => TRUE,
  'authentication_optional' => TRUE,
  'major_version' => 1,
  'minor_version' => 0,
];
