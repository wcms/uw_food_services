<?php

/**
 * @file
 * Contains \RestfulEntityTaxonomyTermCuisines.
 */

/**
 *
 */
class RestfulEntityTaxonomyTermOutletType extends UWRestfulEntityTaxonomyTerm {

  /**
   * {@inheritDoc}
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    $public_fields['description'] = $this->getTaxonomyDescription();

    return $public_fields;
  }

}
