<?php

/**
 * @file
 */

$plugin = [
  'label' => t('Food Services - Outlet Type'),
  'resource' => 'outlet_type',
  'name' => 'outlet_type__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uw_food_outlet_types',
  'description' => t('Export the outlet types types with all authentication providers.'),
  'class' => 'RestfulEntityTaxonomyTermOutletType',
  'authentication_types' => TRUE,
  'authentication_optional' => TRUE,
  'major_version' => 1,
  'minor_version' => 0,
];
