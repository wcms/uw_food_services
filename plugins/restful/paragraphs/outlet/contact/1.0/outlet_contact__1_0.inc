<?php

/**
 * @file
 */

$plugin = [
  'label' => t('Food Services - Outlet Contact'),
  'resource' => 'outlet_contact',
  'name' => 'outlet_contact__1_0',
  'entity_type' => 'paragraphs_item',
  'bundle' => 'contact',
  'description' => t('Export the outlet contacts with all authentication providers.'),
  'class' => 'RestfulEntityParagraphsOutletContact',
  'authentication_types' => TRUE,
  'authentication_optional' => TRUE,
  'major_version' => 1,
  'minor_version' => 0,
];
