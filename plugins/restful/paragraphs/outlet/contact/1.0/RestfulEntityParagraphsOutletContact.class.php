<?php

/**
 * @file
 * Class RestfulEntityParagraphsOutletContact.
 */

/**
 *
 */
class RestfulEntityParagraphsOutletContact extends UWRestfulEntity {

  /**
   *
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['name'] = [
      'property' => 'field_contact_name',
    ];
    $public_fields['position'] = [
      'property' => 'field_contact_title',
      'formatter' => 'json',
    ];
    $public_fields['email'] = [
      'property' => 'field_contact_email',
    ];
    $public_fields['phone'] = [
      'property' => 'field_contact_phone',
      'formatter' => 'json',
    ];

    return $public_fields;
  }

}
