<?php

/**
 * @file
 */

$plugin = [
  'label' => t('Food Services - Franchises Individual Items'),
  'resource' => 'franchise_individual',
  'name' => 'franchise_individual__1_0',
  'entity_type' => 'paragraphs_item',
  'bundle' => 'franchise_individual',
  'description' => t('Export the franchises individual items with all authentication providers.'),
  'class' => 'RestfulEntityParagraphsFranchiseIndividualItems',
  'authentication_types' => TRUE,
  'authentication_optional' => TRUE,
  'major_version' => 1,
  'minor_version' => 0,
];
