<?php

/**
 * @file
 * Contains \RestfulEntityParagraphsFranchiseIndividualItems.
 */

/**
 *
 */
class RestfulEntityParagraphsFranchiseIndividualItems extends UWRestfulEntity {

  /**
   * {@inheritDoc}
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $mapping = [
      'name' => 'field_individual_name',
      'image' => [
        'property' => 'field_individual_photo',
        'process_callbacks' => [
          [$this, 'imageProcess'],
        ],
      ],
      'price' => 'field_individual_price',
      'calories' => 'field_individual_calories',
    ];

    foreach ($mapping as $label => $value) {
      if (is_array($value)) {
        $public_fields[$label] = $value;
      }
      else {
        $public_fields[$label] = [
          'property' => $value,
        ];
      }
    }

    return $public_fields;
  }

}
