<?php

/**
 * @file
 */

$plugin = [
  'label' => t('Food Services - Franchises Combos'),
  'resource' => 'franchise_combos',
  'name' => 'franchise_combos__1_0',
  'entity_type' => 'paragraphs_item',
  'bundle' => 'franchise_combos',
  'description' => t('Export the franchises add ons with all authentication providers.'),
  'class' => 'RestfulEntityParagraphsFranchiseCombos',
  'authentication_types' => TRUE,
  'authentication_optional' => TRUE,
  'major_version' => 1,
  'minor_version' => 0,
];
