<?php

/**
 * @file
 * Contains \RestfulEntityParagraphsFranchiseCombos.
 */

/**
 *
 */
class RestfulEntityParagraphsFranchiseCombos extends UWRestfulEntity {

  /**
   * {@inheritDoc}
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $mapping = [
      'name' => 'field_combo_name',
      'image' => [
        'property' => 'field_combo_image',
        'process_callbacks' => [
          [$this, 'imageProcess'],
        ],
      ],
      'options' => 'field_combo_options',
      'item' => 'field_combo_other',
      'price' => 'field_combo_price',
      'calories' => 'field_combo_calories',
    ];

    foreach ($mapping as $label => $value) {
      if (is_array($value)) {
        $public_fields[$label] = $value;
      }
      else {
        $public_fields[$label] = [
          'property' => $value,
        ];
      }
    }

    return $public_fields;
  }

}
