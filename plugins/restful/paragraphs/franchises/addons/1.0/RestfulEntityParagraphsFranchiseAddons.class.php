<?php

/**
 * @file
 * Contains \RestfulEntityParagraphsFranchiseAddons.
 */

/**
 *
 */
class RestfulEntityParagraphsFranchiseAddons extends UWRestfulEntity {

  /**
   * {@inheritDoc}
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $mapping = [
      'name' => 'field_addon_name',
      'image' => [
        'property' => 'field_addon_photo',
        'process_callbacks' => [
          [$this, 'imageProcess'],
        ],
      ],
      'price' => 'field_addon_price',
      'calories' => 'field_addon_calories',
    ];

    foreach ($mapping as $label => $value) {
      if (is_array($value)) {
        $public_fields[$label] = $value;
      }
      else {
        $public_fields[$label] = [
          'property' => $value,
        ];
      }
    }

    return $public_fields;
  }

}
