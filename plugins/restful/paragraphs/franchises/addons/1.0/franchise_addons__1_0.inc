<?php

/**
 * @file
 */

$plugin = [
  'label' => t('Food Services - Franchises Add Ons'),
  'resource' => 'franchise_addons',
  'name' => 'franchise_addons__1_0',
  'entity_type' => 'paragraphs_item',
  'bundle' => 'franchise_addons',
  'description' => t('Export the franchises add ons with all authentication providers.'),
  'class' => 'RestfulEntityParagraphsFranchiseAddons',
  'authentication_types' => TRUE,
  'authentication_optional' => TRUE,
  'major_version' => 1,
  'minor_version' => 0,
];
