<?php

/**
 * @file
 * Class UWRestfulEntityTaxonomyTerm.
 *
 * Multiple taxonomy terms share same structure, this class is used as parent.
 */

/**
 *
 */
class UWRestfulEntityTaxonomyTerm extends RestfulEntityBaseTaxonomyTerm {

  /**
   * {@inheritDoc}
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['name'] = $public_fields['label'];
    unset($public_fields['label']);

    return $public_fields;
  }

  /**
   * Builds taxonomy description field for term.
   *
   * @return array
   */
  public function getTaxonomyDescription() {
    return [
      'property' => 'description',
      'process_callbacks' => [
        'strip_tags', 'trim',
      ],
    ];
  }

  /**
   * Process callback, Remove Drupal specific items from the image array.
   *
   * @param array $value
   *   The image array.
   *
   * @return array
   *   A cleaned image array.
   */
  public static function imageProcess($value) {
    if (static::isArrayNumeric($value)) {
      $output = [];
      foreach ($value as $item) {
        $output[] = static::imageProcess($item);
      }
      return $output;
    }

    return array(
      'id' => $value['fid'],
      'url' => file_create_url($value['uri']),
      'filemime' => $value['filemime'],
      'filesize' => $value['filesize'],
      'width' => $value['width'],
      'height' => $value['height'],
      'alt' => $value['alt'],
      'title' => $value['title'],
    );
  }

}
